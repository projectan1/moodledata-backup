<?php

class __Mustache_1a65cdf8f0bb975f8d2f7c8cd8974f38 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '
';
        $value = $context->find('hasnavbar');
        $buffer .= $this->section41dabcdb20096ddfeda524efacf5c54f($context, $indent, $value);

        return $buffer;
    }

    private function section41dabcdb20096ddfeda524efacf5c54f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
      <div class="rui-breadcrumbs wrapper-page">
        <div id="page-navbar" class="breadcrumbs-container">
            {{{navbar}}}
        </div>
      </div>
      ';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '      <div class="rui-breadcrumbs wrapper-page">
';
                $buffer .= $indent . '        <div id="page-navbar" class="breadcrumbs-container">
';
                $buffer .= $indent . '            ';
                $value = $this->resolveValue($context->find('navbar'), $context);
                $buffer .= ($value === null ? '' : $value);
                $buffer .= '
';
                $buffer .= $indent . '        </div>
';
                $buffer .= $indent . '      </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
