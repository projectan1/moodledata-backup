<?php

class __Mustache_44dd5ef12fd31c980a0653501791d832 extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '<div class="toast-wrapper mx-auto py-0 fixed-top" role="status" aria-live="polite"></div>
';

        return $buffer;
    }
}
