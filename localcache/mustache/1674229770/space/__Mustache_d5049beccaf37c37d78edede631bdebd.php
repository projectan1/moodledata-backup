<?php

class __Mustache_d5049beccaf37c37d78edede631bdebd extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '<span class="sr-only sr-only-focusable" data-region="jumpto" tabindex="-1"></span>';

        return $buffer;
    }
}
