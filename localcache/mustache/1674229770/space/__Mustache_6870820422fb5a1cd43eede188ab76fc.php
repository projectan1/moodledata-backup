<?php

class __Mustache_6870820422fb5a1cd43eede188ab76fc extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        if ($parent = $this->mustache->loadPartial('mod_forum/discussion_list')) {
            $context->pushBlockContext(array(
                'discussion_create_text' => array($this, 'block37d8bc9de6d0b4a693f699d6ca49f237'),
                'discussion_list_output' => array($this, 'blockA0ed85d9779a3adc0f06eb61791f86f6'),
            ));
            $buffer .= $parent->renderInternal($context, $indent);
            $context->popBlockContext();
        }

        return $buffer;
    }

    private function section60562f50222dfd2822e50e7da0116b1c(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'addanewdiscussion, forum';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'addanewdiscussion, forum';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9821f3665c8e87bfe55cc5620cc39db4(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' discusstopicname, forum, {{subject}} ';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' discusstopicname, forum, ';
                $value = $this->resolveValue($context->find('subject'), $context);
                $buffer .= ($value === null ? '' : call_user_func($this->mustache->getEscape(), $value));
                $buffer .= ' ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionBe0c50e9ce334f65a4b0266831f11cae(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'discussthistopic, forum';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'discussthistopic, forum';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9449e6171de0b6765422ea7d1d74f6cd(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' repliesmany, forum, {{discussionrepliescount}} ';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' repliesmany, forum, ';
                $value = $this->resolveValue($context->find('discussionrepliescount'), $context);
                $buffer .= ($value === null ? '' : call_user_func($this->mustache->getEscape(), $value));
                $buffer .= ' ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionAeeb8638014341c7f353cddbd707a16b(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '<div class="badge badge-light my-1 mx-2">{{#str}} repliesmany, forum, {{discussionrepliescount}} {{/str}}</div>';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= '<div class="badge badge-light my-1 mx-2">';
                $value = $context->find('str');
                $buffer .= $this->section9449e6171de0b6765422ea7d1d74f6cd($context, $indent, $value);
                $buffer .= '</div>';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionE6bd2c4869de9ba46983dd1bc2645b54(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' repliesone, forum, {{discussionrepliescount}} ';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' repliesone, forum, ';
                $value = $this->resolveValue($context->find('discussionrepliescount'), $context);
                $buffer .= ($value === null ? '' : call_user_func($this->mustache->getEscape(), $value));
                $buffer .= ' ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section42cc45d5f43095cedf58b408ca937fab(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' unreadpostsnumber, mod_forum, {{discussionunreadscount}} ';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' unreadpostsnumber, mod_forum, ';
                $value = $this->resolveValue($context->find('discussionunreadscount'), $context);
                $buffer .= ($value === null ? '' : call_user_func($this->mustache->getEscape(), $value));
                $buffer .= ' ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section7187f7ff154a0c5053345766e80157f1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '<div class="badge badge-success my-1 mx-2">{{#str}} unreadpostsnumber, mod_forum, {{discussionunreadscount}} {{/str}}</div>';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= '<div class="badge badge-success my-1 mx-2">';
                $value = $context->find('str');
                $buffer .= $this->section42cc45d5f43095cedf58b408ca937fab($context, $indent, $value);
                $buffer .= '</div>';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA6096d2d75d11f34eb72d9798a6d4215(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = ' unreadpostsone, mod_forum ';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= ' unreadpostsone, mod_forum ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionC274d5b28cf099f3ce9258c914e3397d(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
                       <span class="sep">/</span>
                       <span class="unread"><a href="{{urls.discuss}}#unread">{{#isunreadplural}}<div class="badge badge-success my-1 mx-2">{{#str}} unreadpostsnumber, mod_forum, {{discussionunreadscount}} {{/str}}</div>{{/isunreadplural}}{{^isunreadplural}}<div class="badge badge-success my-1 mx-2">{{#str}} unreadpostsone, mod_forum {{/str}}</div>{{/isunreadplural}}</a></span>';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= '
';
                $buffer .= $indent . '                       <span class="sep">/</span>
';
                $buffer .= $indent . '                       <span class="unread"><a href="';
                $value = $this->resolveValue($context->findDot('urls.discuss'), $context);
                $buffer .= ($value === null ? '' : call_user_func($this->mustache->getEscape(), $value));
                $buffer .= '#unread">';
                $value = $context->find('isunreadplural');
                $buffer .= $this->section7187f7ff154a0c5053345766e80157f1($context, $indent, $value);
                $value = $context->find('isunreadplural');
                if (empty($value)) {
                    
                    $buffer .= '<div class="badge badge-success my-1 mx-2">';
                    $value = $context->find('str');
                    $buffer .= $this->sectionA6096d2d75d11f34eb72d9798a6d4215($context, $indent, $value);
                    $buffer .= '</div>';
                }
                $buffer .= '</a></span>';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionBa00a0e50abff2cba048673c5513d4a1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
           {{< mod_forum/forum_discussion_post }}
               {{$footer}}
                   <div class="link text-sm-left text-md-right">
                       <a href="{{urls.discuss}}"
                          aria-label="{{#str}} discusstopicname, forum, {{subject}} {{/str}}">
                           <i class="far fa-comment-dots mr-1"></i>
                           {{#str}}discussthistopic, forum{{/str}}
                       </a>{{#isreplyplural}}<div class="badge badge-light my-1 mx-2">{{#str}} repliesmany, forum, {{discussionrepliescount}} {{/str}}</div>{{/isreplyplural}}{{^isreplyplural}}<div class="badge badge-light my-1 mx-2">{{#str}} repliesone, forum, {{discussionrepliescount}} {{/str}}</div>{{/isreplyplural}}{{#discussionunreadscount}}
                       <span class="sep">/</span>
                       <span class="unread"><a href="{{urls.discuss}}#unread">{{#isunreadplural}}<div class="badge badge-success my-1 mx-2">{{#str}} unreadpostsnumber, mod_forum, {{discussionunreadscount}} {{/str}}</div>{{/isunreadplural}}{{^isunreadplural}}<div class="badge badge-success my-1 mx-2">{{#str}} unreadpostsone, mod_forum {{/str}}</div>{{/isunreadplural}}</a></span>{{/discussionunreadscount}}
                   </div>
               {{/footer}}
               {{$replyoutput}}{{/replyoutput}}
           {{/ mod_forum/forum_discussion_post }}
       ';
            $result = (string) call_user_func($value, $source, $this->lambdaHelper);
            $buffer .= $result;
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '           ';
                if ($parent = $this->mustache->loadPartial('mod_forum/forum_discussion_post')) {
                    $context->pushBlockContext(array(
                        'footer' => array($this, 'block96b43ad7789beea1fcd07c1c48116a73'),
                        'replyoutput' => array($this, 'blockD41d8cd98f00b204e9800998ecf8427e'),
                    ));
                    $buffer .= $parent->renderInternal($context, $indent);
                    $context->popBlockContext();
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    public function block37d8bc9de6d0b4a693f699d6ca49f237($context)
    {
        $indent = $buffer = '';
        $buffer .= $indent . '        ';
        $value = $context->find('str');
        $buffer .= $this->section60562f50222dfd2822e50e7da0116b1c($context, $indent, $value);
        $buffer .= '
';
    
        return $buffer;
    }

    public function block96b43ad7789beea1fcd07c1c48116a73($context)
    {
        $indent = $buffer = '';
        $buffer .= '                   <div class="link text-sm-left text-md-right">
';
        $buffer .= $indent . '                       <a href="';
        $value = $this->resolveValue($context->findDot('urls.discuss'), $context);
        $buffer .= ($value === null ? '' : call_user_func($this->mustache->getEscape(), $value));
        $buffer .= '"
';
        $buffer .= $indent . '                          aria-label="';
        $value = $context->find('str');
        $buffer .= $this->section9821f3665c8e87bfe55cc5620cc39db4($context, $indent, $value);
        $buffer .= '">
';
        $buffer .= $indent . '                           <i class="far fa-comment-dots mr-1"></i>
';
        $buffer .= $indent . '                           ';
        $value = $context->find('str');
        $buffer .= $this->sectionBe0c50e9ce334f65a4b0266831f11cae($context, $indent, $value);
        $buffer .= '
';
        $buffer .= $indent . '                       </a>';
        $value = $context->find('isreplyplural');
        $buffer .= $this->sectionAeeb8638014341c7f353cddbd707a16b($context, $indent, $value);
        $value = $context->find('isreplyplural');
        if (empty($value)) {
            
            $buffer .= '<div class="badge badge-light my-1 mx-2">';
            $value = $context->find('str');
            $buffer .= $this->sectionE6bd2c4869de9ba46983dd1bc2645b54($context, $indent, $value);
            $buffer .= '</div>';
        }
        $value = $context->find('discussionunreadscount');
        $buffer .= $this->sectionC274d5b28cf099f3ce9258c914e3397d($context, $indent, $value);
        $buffer .= '
';
        $buffer .= $indent . '                   </div>
';
    
        return $buffer;
    }

    public function blockD41d8cd98f00b204e9800998ecf8427e($context)
    {
        $indent = $buffer = '';
    
        return $buffer;
    }

    public function blockA0ed85d9779a3adc0f06eb61791f86f6($context)
    {
        $indent = $buffer = '';
        $value = $context->find('posts');
        $buffer .= $this->sectionBa00a0e50abff2cba048673c5513d4a1($context, $indent, $value);
    
        return $buffer;
    }
}
