<?php

class __Mustache_0ffd374616b430f5478b9ddc7fb53c99 extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '
';
        $buffer .= $indent . '<div class="qbank_statistics discriminative_efficiency">
';
        $buffer .= $indent . '    ';
        $value = $this->resolveValue($context->find('discriminative_efficiency'), $context);
        $buffer .= ($value === null ? '' : $value);
        $buffer .= '
';
        $buffer .= $indent . '</div>
';

        return $buffer;
    }
}
