<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'tiny_media', language 'id', version '4.1'.
 *
 * @package     tiny_media
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['alignment'] = 'Perataan';
$string['alignment_bottom'] = 'Bawah';
$string['alignment_left'] = 'Kiri';
$string['alignment_middle'] = 'Tengah';
$string['alignment_right'] = 'Kanan';
$string['alignment_top'] = 'Atas';
$string['audio'] = 'Audio';
$string['audiosourcelabel'] = 'URL sumber audio';
$string['autoplay'] = 'Main otomatis';
$string['browserepositories'] = 'Jelajah repositori...';
$string['captions'] = 'Takarir';
$string['captions_help'] = 'Teks dapat digunakan untuk mendeskripsikan semua yang terjadi di trek, termasuk suara nonverbal seperti dering telepon.';
$string['constrain'] = 'Ukuran otomatis';
$string['controls'] = 'Tampilkan kontrol';
$string['createmedia'] = 'Sisipkan media';
$string['default'] = 'Bawaan';
$string['deleteselected'] = 'Hapus file yang dipilih';
$string['descriptions'] = 'Deskripsi';
$string['descriptions_help'] = 'Deskripsi audio dapat digunakan untuk memberikan narasi yang menjelaskan detail visual yang tidak terlihat dari audio saja.';
$string['displayoptions'] = 'Opsi display';
$string['enteralt'] = 'Jelaskan gambar ini untuk seseorang yang tidak dapat melihatnya';
$string['enterurl'] = 'Masukkan URL';
$string['height'] = 'Tinggi';
$string['imagebuttontitle'] = 'Gambar';
$string['imageproperties'] = 'Properti gambar';
$string['imageurlrequired'] = 'Gambar harus punya URL';
$string['label'] = 'Label';
$string['languagesavailable'] = 'Bahasa tersedia';
$string['languagesinstalled'] = 'Bahasa terinstal';
$string['link'] = 'Tautan';
$string['loop'] = 'Perulangan';
$string['managefiles'] = 'Kelola berkas';
$string['mediabuttontitle'] = 'Multimedia';
$string['mediamanagerbuttontitle'] = 'Manajer media';
$string['mediamanagerproperties'] = 'Manajer media';
$string['metadata'] = 'Metadata';
$string['metadata_help'] = 'Trek metadata, untuk digunakan dari skrip, hanya dapat digunakan jika pemutar mendukung metadata.';
$string['metadatasourcelabel'] = 'URL trek metadata';
$string['presentation'] = 'Gambar ini hanya dekoratif';
$string['presentationoraltrequired'] = 'Gambar harus memiliki deskripsi, kecuali jika ditandai sebagai dekoratif saja.';
$string['remove'] = 'Hapus';
$string['saveimage'] = 'Simpan gambar';
$string['size'] = 'Ukuran';
$string['srclang'] = 'Bahasa';
$string['subtitles'] = 'Terjemahan';
$string['tracks'] = 'Subtitel dan takarir';
$string['video'] = 'Video';
$string['videoheight'] = 'Tinggi video';
$string['videosourcelabel'] = 'URL sumber video';
$string['videowidth'] = 'Lebar video';
$string['width'] = 'Lebar';
