<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'tiny_h5p', language 'id', version '4.1'.
 *
 * @package     tiny_h5p
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['browserepositories'] = 'Jelajahi repositori ...';
$string['buttontitle'] = 'Konfigurasikan konten H5P';
$string['copyrightbutton'] = 'Tombol hak cipta';
$string['downloadbutton'] = 'Izinkan unduh';
$string['embedbutton'] = 'Sematkan tombol';
$string['h5p:addembed'] = 'Tambahkan H5P tersemat';
$string['h5pfile'] = 'Unggah file H5P';
$string['h5pfileorurl'] = 'URL H5P atau unggahan file';
$string['h5poptions'] = 'Opsi H5P';
$string['h5purl'] = 'URL H5P';
$string['helplinktext'] = 'Pembantu H5P';
$string['insert'] = 'Sisipkan konten H5P';
$string['instructions'] = 'Anda dapat menyisipkan konten H5P dengan <strong> baik </strong> memasukkan URL <strong> atau </strong> dengan mengunggah file H5P.';
$string['invalidh5purl'] = 'URL salah';
$string['modaltitle'] = 'Sisipkan konten H5P';
$string['noh5pcontent'] = 'Tidak ada konten H5P yang ditambahkan';
$string['pluginname'] = 'Sisipan kecil H5P';
$string['privacy:metadata'] = 'Plugin H5P untuk TinyMCE tidak menyimpan data pribadi apa pun.';
