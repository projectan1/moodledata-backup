<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'qbank_columnsortorder', language 'id', version '4.1'.
 *
 * @package     qbank_columnsortorder
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Urutan pengurutan kolom';
$string['privacy:metadata'] = 'Pengaya bank pertanyaan Urutan pengurutan kolom tidak menyimpan data pribadi apapun.';
$string['qbankcolumnname'] = '({$a})';
$string['qbankcolumnsdisabled'] = 'Pengaya bank soal yang dimatikan:';
$string['qbankcolumnsortorder'] = 'Urutan pengurutan kolom';
$string['qbankgotocolumnsort'] = 'Anda dapat mengubah urutan kolom pada bank soal pada halaman {$a}.';
$string['qbankgotomanageqbanks'] = 'Anda dapat menghapus kolom dengan mematikan pengayanya pada <a href=\'{$a}\'>Atur pengaya bank soal</a>.';
$string['qbanksortdescription'] = 'Urutan plugin yang tercantum di bawah menentukan urutan kolom di bank soal.';
