<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'editor_tiny', language 'id', version '4.1'.
 *
 * @package     editor_tiny
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['branding'] = 'Pencitraan merek TinyMCE';
$string['branding_desc'] = 'Dukung TinyMCE dengan menampilkan logo di pojok bawah editor teks. Logo tertaut ke situs web TinyMCE.';
$string['pluginname'] = 'Editor TinyMCE';
$string['privacy:reason'] = 'Editor TinyMCE tidak menyimpan preferensi atau data pengguna.';
$string['settings'] = 'Setelan umum';
$string['tiny:0_characters'] = '{0} karakter';
$string['tiny:0_words'] = '{0} kata';
$string['tiny:accessibility'] = 'Aksibilitas';
$string['tiny:action'] = 'Aksi';
$string['tiny:activity'] = 'Aktivitas';
$string['tiny:address'] = 'Alamat';
$string['tiny:advanced'] = 'Lanjutan';
$string['tiny:align'] = 'Perataan';
$string['tiny:align_center'] = 'Rata tengah';
$string['tiny:align_left'] = 'Rata kiri';
$string['tiny:align_right'] = 'Rata kanan';
$string['tiny:alignment'] = 'Perataan';
$string['tiny:alignment1'] = 'perataan';
$string['tiny:all'] = 'Semua';
$string['tiny:alternative_description'] = 'Deskripsi alternatif';
$string['tiny:alternative_source'] = 'Sumber alternatif';
$string['tiny:alternative_source_url'] = 'URL sumber alternatif';
$string['tiny:anchor'] = 'Jangkar';
$string['tiny:anchor...'] = 'Jangkar...';
$string['tiny:anchors'] = 'Jangkar';
$string['tiny:animals_and_nature'] = 'Hewan dan alam';
$string['tiny:arrows'] = 'Panah';
$string['tiny:austral_sign'] = 'simbol austral';
$string['tiny:b'] = 'B';
$string['tiny:background_color'] = 'Warna latar belakang';
$string['tiny:black'] = 'Hitam';
$string['tiny:block'] = 'Blok';
$string['tiny:blockquote'] = 'Kutipan blok';
$string['tiny:blocks'] = 'Blok';
$string['tiny:blue'] = 'Biru';
$string['tiny:blue_component'] = 'Komponen biru';
$string['tiny:body'] = 'Tubuh';
$string['tiny:bold'] = 'Tebal';
$string['tiny:border'] = 'Batas';
$string['tiny:border_color'] = 'Warna batas';
$string['tiny:border_style'] = 'Gaya batas';
$string['tiny:border_width'] = 'Lebar batas';
$string['tiny:bottom'] = 'Bawah';
$string['tiny:browse_for_an_image'] = 'Telusuri gambar';
$string['tiny:bullet_list'] = 'Daftar poin';
$string['tiny:cancel'] = 'Batal';
$string['tiny:caption'] = 'Takarir';
$string['tiny:cedi_sign'] = 'simbol cedi';
$string['tiny:cell'] = 'Sel';
$string['tiny:cell_properties'] = 'Properti sel';
$string['tiny:cell_spacing'] = 'Spasil sel';
$string['tiny:cell_styles'] = 'Gaya sel';
$string['tiny:cell_type'] = 'Tipe sel';
$string['tiny:center'] = 'Tengah';
$string['tiny:characters'] = 'Karakter';
$string['tiny:characters_no_spaces'] = 'Karakter (tanpa spasi)';
$string['tiny:circle'] = 'Lingkaran';
$string['tiny:class'] = 'Class';
$string['tiny:clear_formatting'] = 'Hapus pemformatan';
$string['tiny:close'] = 'Tutup';
$string['tiny:code'] = 'Kode';
$string['tiny:code_sample...'] = 'Kode contoh...';
$string['tiny:code_view'] = 'Tampilan kode';
$string['tiny:colon_sign'] = 'simbol titik dua';
$string['tiny:color_picker'] = 'Pemilih warna';
$string['tiny:color_swatch'] = 'Contoh warna';
$string['tiny:cols'] = 'Kol';
$string['tiny:column'] = 'Kolom';
$string['tiny:column_clipboard_actions'] = 'Aksi papan klip kolom';
$string['tiny:column_group'] = 'Grup kolom';
$string['tiny:column_header'] = 'Tajuk kolom';
$string['tiny:constrain_proportions'] = 'Batasi proporsi';
$string['tiny:copy'] = 'Salin';
$string['tiny:copy_column'] = 'Salin kolom';
$string['tiny:copy_row'] = 'Salin baris';
$string['tiny:could_not_find_the_specified_string.'] = 'Tidak dapat menemukan string yang ditentukan.';
$string['tiny:could_not_load_emojis'] = 'Tidak dapat memuat emoji';
$string['tiny:count'] = 'Hitung';
$string['tiny:cruzeiro_sign'] = 'simbol cruizero';
$string['tiny:currency'] = 'Mata uang';
$string['tiny:currency_sign'] = 'simbol mata uang';
$string['tiny:current_window'] = 'Jendela sekarang';
$string['tiny:custom...'] = 'Kustom...';
$string['tiny:custom_color'] = 'Warna kustom';
$string['tiny:cut'] = 'Potong';
$string['tiny:cut_column'] = 'Potong kolom';
$string['tiny:cut_row'] = 'Potong baris';
$string['tiny:dark_blue'] = 'Biru gelap';
$string['tiny:dark_gray'] = 'Abu-abu gelap';
$string['tiny:dark_green'] = 'Hijau gelap';
$string['tiny:dark_purple'] = 'Ungu gelap';
$string['tiny:dark_red'] = 'Merah gelap';
$string['tiny:dark_turquoise'] = 'Pirus gelap';
$string['tiny:dark_yellow'] = 'Kuning gelap';
$string['tiny:dashed'] = 'Putus-putus';
$string['tiny:datetime'] = 'Tanggal/waktu';
$string['tiny:decrease_indent'] = 'Kurangi inden';
$string['tiny:default'] = 'Bawaan';
$string['tiny:delete_column'] = 'Hapus kolom';
$string['tiny:delete_row'] = 'Hapus baris';
$string['tiny:delete_table'] = 'Hapus tabel';
$string['tiny:dimensions'] = 'Dimensi';
$string['tiny:disc'] = 'Cakram';
$string['tiny:div'] = 'Div';
$string['tiny:document'] = 'Dokumen';
$string['tiny:dollar_sign'] = 'simbol dolar';
$string['tiny:dong_sign'] = 'simbol dong';
$string['tiny:drachma_sign'] = 'simbol drachma';
$string['tiny:edit'] = 'Edit';
$string['tiny:emojis'] = 'Emoji';
$string['tiny:emojis...'] = 'Emoji...';
$string['tiny:error'] = 'Galat';
$string['tiny:error_form_submit_field_collision.'] = 'Galat: Tabrakan ruas pengiriman formulir.';
$string['tiny:error_no_form_element_found.'] = 'Galat: Formulir elemen tidak ditemukan.';
$string['tiny:euro-currency_sign'] = 'simbol mata uang Euro';
$string['tiny:example'] = 'contoh';
$string['tiny:extended_latin'] = 'Latin diperluas';
$string['tiny:failed_to_initialize_plugin_0'] = 'Gagal menginisialisasi pengaya: {0}';
$string['tiny:failed_to_load_plugin_0_from_url_1'] = 'Gagal memuat pengaya: {0} dari url {1}';
$string['tiny:failed_to_load_plugin_url_0'] = 'Gagal memuat url pengaya: {0}';
$string['tiny:failed_to_upload_image_0'] = 'Gagal mengunggah gambar: {0}';
$string['tiny:file'] = 'Berkas';
$string['tiny:find'] = 'Temukan';
$string['tiny:find_and_replace'] = 'Temukan dan ganti';
$string['tiny:find_and_replace...'] = 'Temukan dan ganti...';
$string['tiny:find_if_searchreplace_plugin_activated'] = 'Temukan (jika pengaya cari dan ganti diaktifkan)';
$string['tiny:find_in_selection'] = 'Temukan dalam seleksi';
$string['tiny:find_whole_words_only'] = 'Temukan seluruh kata saja';
$string['tiny:focus_to_element_path'] = 'Fokus ke jalur elemen';
$string['tiny:focus_to_menubar'] = 'Fokus ke bilah menu';
$string['tiny:focus_to_toolbar'] = 'Fokus ke bilah alat';
$string['tiny:font'] = 'Fonta';
$string['tiny:font_sizes'] = 'Ukuran huruf';
$string['tiny:fonts'] = 'Fonta';
$string['tiny:food_and_drink'] = 'Makanan dan minuman';
$string['tiny:footer'] = 'Footer';
$string['tiny:format'] = 'Format';
$string['tiny:formats'] = 'Format';
$string['tiny:formatting'] = 'pemformatan';
$string['tiny:french_franc_sign'] = 'simbol franch Perancis';
$string['tiny:fullscreen'] = 'Layar penuh';
$string['tiny:g'] = 'G';
$string['tiny:general'] = 'Umum';
$string['tiny:german_penny_symbol'] = 'simbol penny Jerman';
$string['tiny:gray'] = 'Abu-abu';
$string['tiny:green'] = 'Hijau';
$string['tiny:green_component'] = 'Komponen hijau';
$string['tiny:groove'] = 'Groove';
$string['tiny:guarani_sign'] = 'simbol guarani';
$string['tiny:handy_shortcuts'] = 'Pintasan praktis';
$string['tiny:hash'] = '#';
$string['tiny:header'] = 'Tajuk';
$string['tiny:header_cell'] = 'Sel tajuk';
$string['tiny:heading_1'] = 'Tajuk 1';
$string['tiny:heading_2'] = 'Tajuk 2';
$string['tiny:heading_3'] = 'Tajuk 3';
$string['tiny:heading_4'] = 'Tajuk 4';
$string['tiny:heading_5'] = 'Tajuk 5';
$string['tiny:heading_6'] = 'Tajuk 6';
$string['tiny:headings'] = 'Tajuk';
$string['tiny:height'] = 'Tinggi';
$string['tiny:help'] = 'Bantuan';
$string['tiny:hex_color_code'] = 'Kode warna Hex';
$string['tiny:hidden'] = 'Tersembunyi';
$string['tiny:history'] = 'riwayat';
$string['tiny:horizontal_align'] = 'Perataan horizontal';
$string['tiny:horizontal_line'] = 'Baris horizontal';
$string['tiny:horizontal_space'] = 'Spasi horizontal';
$string['tiny:hryvnia_sign'] = 'simbol hryvnia';
$string['tiny:id'] = 'ID';
$string['tiny:id_should_start_with_a_letter_followed_only_by_letters_numbers_dashes_dots_colons_or_'] = 'ID harus dimulai dengan huruf, diikuti hanya dengan huruf, angka, tanda pisah, titik, titik dua, atau garis bawah.';
$string['tiny:image...'] = 'Gambar...';
$string['tiny:image_is_decorative'] = 'Gambar bersifat dekoratif';
$string['tiny:image_list'] = 'Daftar gambar';
$string['tiny:image_title'] = 'Judul gambar';
$string['tiny:imageproxy_http_error_could_not_find_image_proxy'] = 'Kesalahan HTTP ImageProxy: Tidak dapat menemukan Proxy Gambar';
$string['tiny:imageproxy_http_error_incorrect_image_proxy_url'] = 'Kesalahan HTTP ImageProxy: URL Proxy Gambar Salah';
$string['tiny:imageproxy_http_error_rejected_request'] = 'Kesalahan HTTP ImageProxy: Permintaan ditolak';
$string['tiny:imageproxy_http_error_unknown_imageproxy_error'] = 'Kesalahan HTTP ImageProxy: Kesalahan ImageProxy tidak diketahui';
$string['tiny:increase_indent'] = 'Tingkatkan indentasi';
$string['tiny:indentation'] = 'Indentasi';
$string['tiny:indian_rupee_sign'] = 'simbol rupee India';
$string['tiny:inline'] = 'Inline';
$string['tiny:insert'] = 'Sisip';
$string['tiny:insert_column_after'] = 'Sisip kolom setelah';
$string['tiny:insert_column_before'] = 'Sisip kolom sebelum';
$string['tiny:insert_datetime'] = 'Sisip tanggal/waktu';
$string['tiny:insert_image'] = 'Sisip gambar';
$string['tiny:insert_link_if_link_plugin_activated'] = 'Sisip tautan (jika pengaya diaktifkan)';
$string['tiny:insert_row_after'] = 'Sisip baris setelah';
$string['tiny:insert_row_before'] = 'Sisip baris sebelum';
$string['tiny:insert_table'] = 'Sisip tabel';
$string['tiny:insert_template'] = 'Sisip templat';
$string['tiny:insert_template...'] = 'Sisip templat...';
$string['tiny:insert_video'] = 'Sisip video';
$string['tiny:insertedit_code_sample'] = 'Sisipkan/edit contoh kode';
$string['tiny:insertedit_image'] = 'Sisipkan/edit gambar';
$string['tiny:insertedit_media'] = 'Sisipkan/edit media';
$string['tiny:insertedit_video'] = 'Sisipkan/edit media';
$string['tiny:inset'] = 'Sisipan';
$string['tiny:italic'] = 'Miring';
$string['tiny:keyboard_navigation'] = 'Navigasi papan ketik';
$string['tiny:kip_sign'] = 'simbol kip';
$string['tiny:language'] = 'Bahasa';
$string['tiny:left'] = 'Kiri';
$string['tiny:left_to_right'] = 'Kiri ke kanan';
$string['tiny:light_blue'] = 'Biru muda';
$string['tiny:light_gray'] = 'Abu-abu muda';
$string['tiny:light_green'] = 'Hijau muda';
$string['tiny:light_purple'] = 'Ungu muda';
$string['tiny:link...'] = 'Tautan...';
$string['tiny:link_list'] = 'Daftar tautan';
$string['tiny:lira_sign'] = 'simbol lira';
$string['tiny:livre_tournois_sign'] = 'simbol livre tournols';
$string['tiny:manat_sign'] = 'simbol manat';
$string['tiny:mill_sign'] = 'simbol mill';
$string['tiny:naira_sign'] = 'simbol naira';
$string['tiny:new_sheqel_sign'] = 'simbol sheqel baru';
$string['tiny:nordic_mark_sign'] = 'simbol mark nordic';
$string['tiny:peseta_sign'] = 'simbol peseta';
$string['tiny:peso_sign'] = 'simbol peso';
$string['tiny:ruble_sign'] = 'simbol ruble';
$string['tiny:rupee_sign'] = 'simbol rupee';
$string['tiny:spesmilo_sign'] = 'simbol spesmilo';
$string['tiny:tenge_sign'] = 'simbol tenge';
$string['tiny:tugrik_sign'] = 'simbol tugrik';
$string['tiny:turkish_lira_sign'] = 'simbol lira Turki';
$string['tiny:won_sign'] = 'simbol won';
