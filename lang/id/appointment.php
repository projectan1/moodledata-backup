<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'appointment', language 'id', version '4.1'.
 *
 * @package     appointment
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['appointment:viewcancellations'] = 'Lihat pembatalan';
$string['appointmentactivity'] = 'Aktivitas janji bertemu';
$string['approvalreqd'] = 'Persetujuan diperlukan';
$string['approvalreqd_help'] = 'Ketika "Persetujuan diperlukan" dicentang, peserta akan membutuhkan persetujuan dari manajer mereka untuk diizinkan menghadiri sesi janji bertemu';
$string['approve'] = 'Menyetujui';
$string['attendance'] = 'Kehadiran';
$string['attendanceinstructions'] = 'Pilih pengguna yang menghadiri sesi:';
$string['attendedsession'] = 'Sesi yang dihadiri';
$string['attendees'] = 'Hadirin';
$string['attendeestablesummary'] = 'Orang yang berencana atau pernah menghadiri sesi ini.';
$string['availableseats'] = 'Tersedia {$ a} kursi';
$string['book'] = 'Memesan';
$string['bookedvscapacity'] = 'Dipesan / Kapasitas';
$string['bookingcancelled'] = 'Pemesanan Anda telah dibatalkan.';
$string['bookingcompleted'] = 'Pemesanan Anda telah selesai.';
$string['bookingfull'] = 'Penuh';
$string['bookingopen'] = 'Buka';
$string['break'] = 'Waktu istirahat';
$string['break_help'] = 'Pengaturan ini menentukan waktu istirahat antar sesi. Menyetel ke 0 berarti tidak perlu istirahat.';
$string['calendareventdescriptionbooking'] = 'Anda dipesan untuk ini<a href="{$an}"> Sesi janji bertemu </a> .';
$string['calendareventdescriptionsession'] = 'Anda telah membuat <a href="{$an}"> Sesi janji bertemu </a> ini.';
$string['calendaroptions'] = 'Opsi kalender';
$string['cancel'] = 'Membatalkan';
$string['cancelbooking'] = 'Batalkan pemesanan';
$string['cancellationinstrmngr'] = '# Pemberitahuan untuk manajer';
$string['cancellationinstrmngr_help'] = 'Ketika ** Kirim pemberitahuan ke manajer ** dicentang, teks di bidang ** Pemberitahuan untuk manajer ** dikirim ke manajer pelajar yang memberi tahu bahwa mereka telah membatalkan pemesanan janji temu.';
$string['cancellationmessage'] = 'Pesan pembatalan';
$string['cancellationmessage_help'] = 'Pesan ini dikirim setiap kali pengguna membatalkan pemesanan mereka untuk suatu sesi.';
$string['cancellations'] = 'Pembatalan';
$string['cancellationstablesummary'] = 'Daftar orang yang telah membatalkan pendaftaran sesi mereka.';
$string['cancelreason'] = 'Alasan pembatalan';
$string['cannotapproveatcapacity'] = 'Anda tidak dapat menyetujui peserta lagi karena sesi ini sudah penuh.';
$string['cannotsignupsessioninprogress'] = 'Anda tidak dapat mendaftar, sesi ini sedang berlangsung';
$string['cannotsignupsessionover'] = 'Anda tidak dapat mendaftar, sesi ini sudah berakhir.';
$string['capacity'] = 'Kapasitas';
$string['capacity_help'] = '** Kapasitas ** adalah jumlah kursi yang tersedia dalam satu sesi.

Ketika sesi Janji bertemu mencapai kapasitasnya, detail sesi tidak muncul di halaman kursus. Rincian akan tampak abu-abu di halaman \'Lihat semua sesi\' dan pelajar tidak dapat mendaftar di sesi tersebut.
& nbsp;';
$string['closed'] = 'Tutup';
$string['confirmationinstrmngr'] = '# Pemberitahuan untuk manajer';
$string['confirmationinstrmngr_help'] = 'Saat "Kirim pemberitahuan ke manajer" dicentang, teks di bidang "Pemberitahuan untuk manajer" dikirim ke manajer yang memberitahukan bahwa anggota staf telah mendaftar untuk sesi janji bertemu.';
$string['confirmationmessage'] = 'pesan konfirmasi';
$string['confirmationmessage_help'] = 'Pesan ini dikirim setiap kali pengguna mendaftar untuk sesi.';
$string['confirmcancelbooking'] = 'Konfirmasikan pembatalan';
$string['courseresetsessions'] = 'Hapus semua sesi';
$string['courseresetsignups'] = 'Hapus semua pendaftaran sesi';
$string['crontask'] = 'Pekerjaan pemeliharaan janji';
$string['currentstatus'] = 'Status terkini';
$string['customisednotifications'] = 'Pemberitahuan yang disesuaikan';
$string['date'] = 'Tanggal';
$string['decidelater'] = 'Putuskan Nanti';
$string['decline'] = 'Menurun';
$string['delete'] = 'Menghapus';
$string['deleteappointmentconfirm'] = 'Apakah Anda benar-benar yakin ingin menghapus janji bertemu ini dan semua pemesanannya?';
$string['deletesession'] = 'Hapus sesi';
$string['deletetimeframe'] = 'Hapus jangka waktu';
$string['details'] = 'Detail';
$string['duplicate'] = 'Duplikat';
$string['duplicateappointment'] = 'Janji rangkap';
$string['email:message'] = 'Pesan';
$string['email:subject'] = 'Subyek';
$string['emailmanager'] = 'Kirim pemberitahuan ke manajer';
$string['emailmanagercancellation'] = '# Kirim pemberitahuan ke manajer';
$string['emailmanagercancellation_help'] = 'Ketika "Kirim pemberitahuan ke manajer" dicentang, email akan dikirim ke manajer pelajar yang memberi tahu mereka bahwa pemesanan janji bertemu telah dibatalkan.';
$string['emailmanagerconfirmation'] = '# Kirim pemberitahuan ke manajer';
$string['emailmanagerconfirmation_help'] = 'Saat "Kirim pemberitahuan ke manajer" dicentang, email konfirmasi akan dikirim ke manajer pelajar saat pelajar mendaftar untuk sesi Janji bertemu.';
$string['emailmanagerreminder'] = '# Kirim pemberitahuan ke manajer';
$string['empty'] = 'Kosong';
$string['endtime'] = 'Akhir waktu';
$string['error:cancellationsnotallowed'] = 'Anda tidak diizinkan untuk membatalkan pendaftaran ini.';
$string['error:canttakeattendanceforunstartedsession'] = 'Tidak dapat mengambil kehadiran untuk sesi yang belum dimulai.';
$string['error:eventoccurred'] = 'Anda tidak dapat membatalkan acara yang sudah terjadi.';
$string['error:invaliduserid'] = 'ID pengguna tidak valid';
$string['error:manageremailaddressmissing'] = 'Anda saat ini tidak ditugaskan ke manajer dalam sistem. Silakan hubungi administrator situs.';
$string['error:sessionsplitexceeds'] = 'Waktu pemisahan sesi melebihi durasi sesi.';
$string['error:sessionstartafterend'] = 'Waktu mulai sesi lebih lambat dari waktu akhir sesi.';
$string['errorcannoteditsessions'] = 'Anda tidak memiliki izin untuk mengedit sesi';
$string['errorcannotviewappointment'] = 'Anda tidak memiliki izin untuk melihat janji bertemu ini';
$string['errorcannotviewattendees'] = 'Anda tidak memiliki izin untuk melihat peserta';
$string['eventaddsession'] = 'Sesi ditambahkan';
$string['eventapproverequests'] = 'Sesi menyetujui permintaan';
$string['eventattendancetaken'] = 'Kehadiran sesi diambil';
$string['eventattendeesviewed'] = 'Peserta sesi dilihat';
$string['eventdeletesession'] = 'Sesi dihapus';
$string['eventsignup'] = 'Pendaftaran sesi';
$string['eventupdatesession'] = 'Sesi diperbarui';
$string['full'] = 'Tanggal terisi penuh';
$string['fullfilter'] = 'Penuh';
$string['goback'] = 'Kembali';
$string['icalendarheading'] = 'Lampiran iCalendar';
$string['joinwaitlist'] = 'Bergabunglah dengan daftar tunggu';
$string['location'] = 'Lokasi';
$string['managecustomfields'] = 'Kelola bidang khusus';
$string['multipleappointments'] = 'Banyak janji bertemu';
$string['noactionableunapprovedrequests'] = 'Tidak ada permintaan tidak disetujui yang dapat ditindaklanjuti';
$string['noremindersneedtobesent'] = 'Tidak ada pengingat perlu dikirim.';
$string['nosignedupusers'] = 'Tidak ada pengguna yang mendaftar untuk sesi ini.';
$string['notset'] = 'Tidak diatur';
$string['notsignedup'] = 'Anda tidak terdaftar untuk sesi ini.';
$string['partiallyfull'] = 'Sebagian penuh';
$string['pluginname'] = 'Janji';
$string['potentialattendees'] = 'Calon Peserta';
$string['privacy:metadata:appointment_signups'] = 'Pengguna mendaftar ke sesi Janji bertemu';
$string['privacy:metadata:appointment_signups:id'] = 'ID pendaftaran';
$string['privacy:metadata:appointment_signups:mailedreminder'] = 'Waktu pengingat terakhir dikirim';
$string['privacy:metadata:appointment_signups:sessionid'] = 'ID sesi';
$string['privacy:metadata:appointment_signups_status'] = 'Status pengguna yang mendaftar ke sebuah sesi';
$string['privacy:metadata:appointment_signups_status:grade'] = 'Nilai yang diberikan untuk menghadiri sesi';
$string['privacy:metadata:appointment_signups_status:note'] = 'Menyimpan alasan sesi yang dibatalkan';
$string['privacy:metadata:appointment_signups_status:signupid'] = 'ID pendaftaran';
$string['privacy:metadata:appointment_signups_status:statuscode'] = 'Status pendaftaran mis. Dibatalkan, ditolak, dijaga sepenuhnya';
$string['privacy:metadata:appointment_signups_status:timecreated'] = 'Waktu pendaftaran dibuat.';
$string['remindermessage'] = 'Pesan pengingat';
$string['remindermessage_help'] = 'Pesan ini dikirim beberapa hari sebelum tanggal mulai sesi.';
$string['reminderperiod'] = 'Beberapa hari sebelum pesan dikirim';
$string['reminderperiod_help'] = 'Pesan pengingat akan dikirim beberapa hari ini sebelum dimulainya sesi.';
$string['requestmessage'] = 'Minta pesan';
$string['requeststablesummary'] = 'Orang yang meminta untuk menghadiri sesi ini.';
$string['room'] = 'Ruangan';
$string['saveattendance'] = 'Simpan kehadiran';
$string['seatsavailable'] = 'Kursi tersedia';
$string['sessionavailability'] = 'Ketersediaan sesi';
$string['sessiondatetime'] = 'Tanggal / waktu sesi';
$string['sessiondescription'] = 'Deskripsi';
$string['sessionfinished'] = 'Selesai';
$string['sessionfinishtime'] = 'Waktu selesai sesi';
$string['sessioninprogress'] = 'sesi sedang berlangsung';
$string['sessionisfull'] = 'Sesi ini sekarang sudah penuh. Anda perlu memilih waktu lain atau berbicara dengan instruktur.';
$string['sessionrequiresmanagerapproval'] = 'Sesi ini membutuhkan persetujuan manajer untuk memesan.';
$string['sessions'] = 'Sesi';
$string['sessionsdetailstablesummary'] = 'Deskripsi lengkap dari sesi saat ini.';
$string['sessionstartdate'] = 'Tanggal mulai sesi';
$string['sessionstarttime'] = 'Waktu mulai sesi';
$string['setting:oneemailperday'] = 'Kirim beberapa email konfirmasi untuk acara beberapa hari.';
$string['setting:oneemailperday_caption'] = 'Satu pesan per hari:';
$string['settings'] = 'Pengaturan';
$string['shortname'] = '# Nama pendek';
$string['shortname_help'] = '** Nama pendek ** adalah deskripsi sesi yang muncul di kalender pelatihan ketika ** Tampilkan di kalender ** diaktifkan.';
$string['showoncalendar'] = 'Pengaturan tampilan kalender';
$string['sign-ups'] = 'Mendaftar';
$string['signupforthissession'] = 'Mendaftar untuk sesi janji bertemu ini';
$string['split'] = 'Pembagian otomatis';
$string['split_help'] = 'Pengaturan ini menentukan durasi setiap sesi yang dibuat dalam jangka waktu tersebut. Menyetelnya ke 0 menonaktifkan opsi.';
$string['status'] = 'Status';
$string['status_approved'] = 'Disetujui';
$string['status_declined'] = 'Ditolak';
$string['status_fully_attended'] = 'Sepenuhnya dihadiri';
$string['status_no_show'] = 'Tidak ada pertunjukan';
$string['status_partially_attended'] = 'Hadir sebagian';
$string['status_requested'] = 'Diminta';
$string['status_user_cancelled'] = 'Pengguna Dibatalkan';
$string['status_waitlisted'] = 'Dalam daftar tunggu';
$string['suppressemail'] = 'Sembunyikan pemberitahuan email';
$string['takeattendance'] = 'Ambil kehadiran';
$string['thirdpartyemailaddress'] = 'Alamat email pihak ketiga';
$string['thirdpartywaitlist'] = 'Beri tahu pihak ketiga tentang sesi dalam daftar tunggu';
$string['timecancelled'] = 'Waktu Dibatalkan';
$string['timerequested'] = 'Waktu Diminta';
$string['timesignedup'] = 'Waktu Mendaftar';
$string['timestart'] = 'Waktu mulai';
$string['unapprovedrequests'] = 'Permintaan yang Tidak Disetujui';
$string['unknowndate'] = '(tanggal tidak diketahui)';
$string['unknowntime'] = '(waktu tidak diketahui)';
$string['usercalentry'] = 'Tampilkan entri di kalender pengguna';
$string['usercancelledon'] = 'Pengguna dibatalkan pada {$a}';
$string['usernotsignedup'] = 'Status: tidak terdaftar';
$string['usersignedup'] = 'Status: mendaftar';
$string['usersignedupon'] = 'Pengguna mendaftar di {$a}';
$string['venue'] = 'Lokasi';
$string['waitlistedmessage'] = 'Pesan dalam daftar tunggu';
$string['waitlistedmessage_help'] = 'Pesan ini dikirim setiap kali pengguna mendaftar untuk sesi daftar tunggu.';
