<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'label', language 'id', version '4.1'.
 *
 * @package     label
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['configdndmedia'] = 'Tawarkan untuk membuat Area Teks dan media saat berkas media diseret dan dilepaskan ke kursus.';
$string['configdndresizeheight'] = 'Saat Area Teks dan media dibuat dari berkas media yang diseret dan dilepaskan, ubah ukurannya jika lebih tinggi dari banyak piksel ini. Jika disetel ke nol, berkas media tidak akan diubah ukurannya.';
$string['configdndresizewidth'] = 'Saat sumber Teks dan media dibuat dari berkas media yang diseret dan dilepas, ubah ukurannya jika lebih lebar dari banyak piksel ini. Jika disetel ke nol, berkas media tidak akan diubah ukurannya.';
$string['dndmedia'] = 'Seret dan lepas media';
$string['dndresizeheight'] = 'Atur tinggi setelan seret dan lepas';
$string['dndresizewidth'] = 'Atur lebar setelan seret dan lepas';
$string['dnduploadlabel'] = 'Tambahkan media pada halaman kursus';
$string['dnduploadlabeltext'] = 'Tambahkan Area Teks dan media ke halaman kursus';
$string['indicator:cognitivedepth'] = 'Area Teks dan media kognitif';
$string['indicator:cognitivedepth_help'] = 'Indikator ini didasarkan pada kedalaman kognitif yang dicapai oleh siswa dalam Area teks dan sumber media.';
$string['indicator:cognitivedepthdef'] = 'Area Teks dan media kognitif';
$string['indicator:cognitivedepthdef_help'] = 'Peserta telah mencapai persentase keterlibatan kognitif yang ditawarkan oleh Area Teks dan sumber media selama interval analisis ini (Tingkat = Tanpa tampilan, Tampilan)';
$string['indicator:socialbreadth'] = 'Area Teks dan media sosial';
$string['indicator:socialbreadth_help'] = 'Indikator ini didasarkan pada keluasan sosial yang dicapai oleh siswa dalam sumber Label.';
$string['indicator:socialbreadthdef'] = 'Label sosial';
$string['indicator:socialbreadthdef_help'] = 'Peserta telah mencapai persentase keterlibatan sosial yang ditawarkan oleh sumber daya Label selama interval analisis ini (Tingkat = Tidak ada partisipasi, Peserta saja)';
$string['label:addinstance'] = 'Tambahkan label';
$string['label:view'] = 'Lihat label';
$string['labeltext'] = 'Teks';
$string['modulename'] = 'Teks dan media';
$string['modulename_help'] = 'Area Teks dan media memungkinkan Anda untuk menampilkan teks dan multimedia pada halaman kursus.

Anda dapat menggunakan Area Teks dan media untuk:

* Pisahkan daftar panjang kegiatan kursus dengan subjudul atau gambar
* Menampilkan video tersemat langsung di halaman kursus
* Tambahkan deskripsi singkat ke bagian kursus';
$string['modulenameplural'] = 'Area teks dan media';
$string['pluginadministration'] = 'Administrasi area teks dan media';
$string['pluginname'] = 'Teks dan media';
$string['privacy:metadata'] = 'Pengaya Label tidak menyimpan data pribadi apa pun.';
$string['search:activity'] = 'Area teks dan media';
